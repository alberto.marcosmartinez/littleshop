﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LittleShop.Startup))]
namespace LittleShop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
