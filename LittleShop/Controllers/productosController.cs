﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LittleShop.Models;

namespace LittleShop.Controllers
{
    public class productosController : Controller
    {
        private TiendaEntities db = new TiendaEntities();

        // GET: productos
        public ActionResult Index()
        {
            var productos = db.productos.Include(p => p.proveedore);
            return View(productos.ToList());
        }

        // GET: productos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: productos/Create
        public ActionResult Create()
        {
            ViewBag.id_proveedor = new SelectList(db.proveedores, "id_proveedor", "nombre_proveedor");
            return View();
        }

        // POST: productos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_producto,producto_nombre,producto_precio,producto_cantidad,producto_descripcion,id_proveedor")] producto producto)
        {
            if (ModelState.IsValid)
            {
                db.productos.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_proveedor = new SelectList(db.proveedores, "id_proveedor", "nombre_proveedor", producto.id_proveedor);
            return View(producto);
        }

        // GET: productos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_proveedor = new SelectList(db.proveedores, "id_proveedor", "nombre_proveedor", producto.id_proveedor);
            return View(producto);
        }

        // POST: productos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_producto,producto_nombre,producto_precio,producto_cantidad,producto_descripcion,id_proveedor")] producto producto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_proveedor = new SelectList(db.proveedores, "id_proveedor", "nombre_proveedor", producto.id_proveedor);
            return View(producto);
        }

        // GET: productos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            producto producto = db.productos.Find(id);
            db.productos.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: productos/Edit/5
        public ActionResult VentaProductos(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_proveedor = new SelectList(db.proveedores, "id_proveedor", "nombre_proveedor", producto.id_proveedor);
            return View(producto);
        }

        // POST: productos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VentaProductos([Bind(Include = "id_producto,producto_nombre,producto_precio,producto_cantidad,producto_descripcion,id_proveedor")] producto producto, int cantidadProducto)
        {
            if (ModelState.IsValid)
            {

                producto.producto_cantidad = producto.producto_cantidad - cantidadProducto;

                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_proveedor = new SelectList(db.proveedores, "id_proveedor", "nombre_proveedor", producto.id_proveedor);
            return View(producto);
        }

        public ActionResult precioVenta(int id_producto, int cantidad_producto)
        {
            var coste = (from producto in db.productos
                         where producto.id_producto == id_producto
                         select producto.producto_precio).FirstOrDefault();

            int valor = int.Parse(coste.ToString());

            return Content((valor * cantidad_producto).ToString());
        }
    }
}
